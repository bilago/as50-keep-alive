﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AS50KeepAlive
{
    static class Program
    {
        static bool startMinimized = false;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
                startMinimized = true;
                
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(startMinimized));
        }
        
    }
}
