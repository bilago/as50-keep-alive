﻿using CSCore.CoreAudioAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NAudio.Wave;
using System.IO;
using System.Windows.Resources;
using System.Reflection;
//using System.Media;
using System.Diagnostics;
using Microsoft.Win32;

namespace AS50KeepAlive
{
    public partial class Form1 : Form
    {
        //Public bool to let the app know if it needs to start minimized (run at startup)
        public bool StartMinimized = false;

        //using a stopwatch to monitor the length of time since it's more reliable than dateTime
        public Stopwatch sw = new Stopwatch();

        //Running the timer on a separate thread to prevent the UI from locking
        public static Thread timerView;

        //Public bool to help terminate "sleepytime" before it's finished
        public static bool timeToQuit = false;

        //The Registry Path to windows startup
        private RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        //Registry Entry name
        private string myApp = "AS50_SoundFix";

        public bool timeToBreak = false;
        private string nextSound = "WhiteNoise";

        //Audio variables to solve memory leak issue
        public static WaveOut waveOut;
        public Stream mp3file;
        public Stream whiteNoise;
        public Stream beep;
        public Mp3FileReader mp3Reader;
        public Mp3FileReader whiteNoiseReader;
        public Mp3FileReader beepReader;
        public WaveStream blockAlignedStream;
        AudioSessionManager2 sessionManager;
        AudioMeterInformation audioMeterInformation;
        private BlockAlignReductionStream whiteNoiseBlockAlignStream;
        private BlockAlignReductionStream beepBlockAlignStream;


        /// <summary>
        /// Entry Method to the Form
        /// </summary>
        /// <param name="startMinimized">True to send form to system tray</param>
        public Form1(bool startMinimized)
        {
            
            if (Properties.Settings.Default.UpdateSettings)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                Properties.Settings.Default.Save();
            }
            
            InitializeComponent();
            StartMinimized = startMinimized;            
        }

        /// <summary>
        /// Event is raised when the Form is first shown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Shown(object sender, EventArgs e)
        {
            label5.Text = string.Format("v{0}", Assembly.GetEntryAssembly().GetName().Version);
            this.checkBox1.CheckedChanged -= checkBox1_CheckedChanged;
            checkBox1.Checked = checkStatus();
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            numericUpDown1.Value = Properties.Settings.Default.Interval;
            waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
            whiteNoise = GetResourceStream("whitenoise.mp3");
            beep = GetResourceStream("lowBeep.mp3");
            whiteNoiseReader = new Mp3FileReader(whiteNoise);
            beepReader = new Mp3FileReader(beep);
            whiteNoiseBlockAlignStream = new BlockAlignReductionStream(
                            WaveFormatConversionStream.CreatePcmStream(
                                whiteNoiseReader));
            beepBlockAlignStream = new BlockAlignReductionStream(
                            WaveFormatConversionStream.CreatePcmStream(
                                beepReader));

            initializeAudioStream();
           

            if (StartMinimized)
                this.WindowState = FormWindowState.Minimized;
    
            //Running the audio detection on its own asynchronous thread
            backgroundWorker1.RunWorkerAsync();
        }

        //===============================================
        //===============================================
        //===============================================
        //Throwing all events here

        /// <summary>
        /// Event is rasied when Form is loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Form closing Event, gives us a chance to tell the other threads that they need to stop, gracefully.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            destroyThread(true);
            Application.Exit();
        }

        //this is the background worker event
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundWorker1.WorkerSupportsCancellation = true;
            onTick();
        }

        //event is raised when the form is closing , iniated by the user
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            destroyThread(true);
        }

        //form resize event, lets us minimize to system tray or to restore back
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                this.Hide();
                this.ShowInTaskbar = false;
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.Show();
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
                this.BringToFront();
            }
        }

        //event isn't needed anymore
        private void Form1_Activated(object sender, EventArgs e)
        { }

        //raises an event when you click the system tray icon
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// This is the event that's raised when the timer reaches it's set interval to display the time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            //Lets manage our own thread instead of a backgorund worker.
            if(timerView!=null)
                if (timerView.IsAlive)
                    return;
            
            timerView = new Thread(refreshTimer);
            timerView.IsBackground = true;           
            timerView.Start();
            //GC.Collect();
        }

        //Looks like the user clicked the checkbox
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            toggleIt();
        }

        //Event that triggers when the value in the numeric box changes
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //Lets save the settings for when they restart the app
            Properties.Settings.Default.Interval = numericUpDown1.Value;
            Properties.Settings.Default.Save();
        }

        //Let's bring them to the donate page
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://goo.gl/Jz8wRi");
        }

        //This is the button that will restart everything without quitting the application
        private void button1_Click(object sender, EventArgs e)
        {
            performButton1();
        }

       

        //End Events
        //===============================================
        //===============================================
        //===============================================

        public int mslength;
        public int refreshRate = 125;
        /// <summary>
        /// Sleep without blocking UI
        /// </summary>
        /// <param name="length">Length to sleep, in minutes</param>
        /// <param name="minutes">True means that the value passed is in minute value, else its in seconds</param>
        private void sleepyTime(int length,bool minutes, bool dontCheckAudio)
        {            
            try
            {
                //lets convert to milliseconds:
                if (minutes)
                    mslength = length * 60 * 1000;
                else                    
                    mslength = length * 1000;

                //now that mslength is set, lets divide that number by refreshRate ticks:
                mslength = mslength / refreshRate;
                int interval = 0;
                //mslength should now be the amount of ticks required to process the sleep command
                for (int i = 0; i < (mslength); i++)
                {
                    interval++;

                    Application.DoEvents();

                    Thread.Sleep(refreshRate);

                    if (!dontCheckAudio)
                    {
                        if (interval == 4)
                        {
                            interval = 0;
                            if (newCheckAudio())
                            {
                                setLabel2("Audio was detected, waking up.");
                                i = mslength;
                            }
                        }
                        
                    if (sw.IsRunning)
                        if (sw.Elapsed.TotalMinutes >= (Decimal.ToInt32(numericUpDown1.Value) - 5))
                            i = mslength;
                    }

                    Thread.Sleep(refreshRate);
                   
                    if (timeToQuit)
                    {
                        timeToQuit = false;
                        return;
                    }
                    if (timeToBreak)
                    {
                        timeToBreak = false;
                        break;
                    }
                    if (backgroundWorker1.CancellationPending)
                        break;
                    Application.DoEvents();
                    //GC.Collect();
                }
                             
            }
            catch
            {
                //This shouldn't happen but just to be safe lets have a catch fallback
                setLabel2("Status: A problem was detected, retrying");                
            }
        }

        //This is the meat and juice of the tool, put it in a never ending loop so the background worker doesn't have to be restarted every 4 minutes. That was too unstable. This worked for an overnight test (8 hours)
        private void onTick()
        {
            while (true)
            {
                setLabel2("Status: Checking if Audio is still playing");  
                if (!sw.IsRunning)
                    sw.Start();

                //This is going to check for audio playing. It will now not detect audio playing but on mute.
                if (newCheckAudio())
                {
                    //sleeping 4 minutes to check if we need to wake up the headset right before it turns off at the 5 minute mark
                    setLabel2("Status: Audio Detected, sleeping 4 minutes");                    
                    sw.Stop();
                    sw.Reset();
                    sleepyTime(4, true, true);                 
                }
                else
                {
                    setLabel8("");
                    //if audio isn't playing but the timer is closer to the user set value than 5 minutes, lets stop waking up the headset to let it go back to sleep automatically
                    if (sw.Elapsed.TotalMinutes > (Decimal.ToInt32(numericUpDown1.Value) - 5))
                    {
                        
                        
                        playAudio(false);
                        setLabel2("Status: Audio Not detected, reaching timelimit, sleeping until audio plays");
                        Application.DoEvents();
                        sw.Stop();
                        sw.Reset();

                        sleepyTime(5, true, false);
                        
                        sw.Start();                        
                    }
                    else
                    {
                        //No Audio detected, lets play a brief sound and then take a 4 minute rest before checking again
                        setLabel2("Status: Audio Not detected, time limit not hit, playing audio...");                                         
                        playAudio(true);
                        
                        Application.DoEvents();
                        int sleepMore = 4;
                        setLabel2(string.Format("Status: Audio Not detected... sleeping for {0} minutes.", sleepMore.ToString()));                        
                        sleepyTime(sleepMore, true, false);
                                        
                    }
                    Application.DoEvents();
                    //This will monitor for any request to cancel action, either by quitting the application or by restarting the timer with the button
                    if (backgroundWorker1.CancellationPending)
                        break;
                }
            }
        }
        
        /// <summary>
        /// This provides a thread safe way of editing the status label
        /// </summary>
        /// <param name="text">Text you want to pass to the label</param>
        private void setLabel2(string text)
        {
            try
            {
                label2.Invoke((MethodInvoker)(() =>
                 { label2.Text = text; }));
            }
            catch { }
        }

        private void setLabel8(string text)
        {
            try
            {
                label8.Invoke((MethodInvoker)(() =>
                { label8.Text = text; }));
            }
            catch { }
        }
       
        //This is the method the timer thread will process, made sure it was simple enough to not cause any issues
        private void refreshTimer()
        {
           
            //Time to be thread safe here...
            lock (label4) 
            {
                if (sw.IsRunning)
                    label4.Invoke((MethodInvoker)(() => label4.Text = sw.Elapsed.TotalMinutes.ToString("00.00")));                   
                else 
                    label4.Invoke((MethodInvoker)(() => label4.Text = "Timer is sleeping..."));
                //There is a slight issue with the background workier cancelling, when this happens I'm flagging the label as "wait" so if it's caught by this thread it will attempt again to restart the backgorund worker. Seems to be working
                if (label2.Text.Contains("wait"))
                {
                    performButton1();
                }
           
            }
            Application.DoEvents();
        }                

        //This is the method called by pressing the button, and also by the timer2 thread when things go wrong
        private void performButton1()
        {
            sw.Stop();
            sw.Reset();
            destroyThread(false);
            //backgroundWorker1.CancelAsync();
            timeToBreak = true;
            //GC.Collect();
            if (!backgroundWorker1.IsBusy)
                backgroundWorker1.RunWorkerAsync();
            sw.Start();
        }

        /// <summary>
        /// Checks the registry to see if it's set to run this application at startup. user needs administrative privs to use this
        /// </summary>
        /// <returns></returns>
        public bool checkStatus()
        {
            try
            {
                // Check to see the current state (running at startup or not)
                if (rkApp.GetValue(myApp) == null)
                {
                    // The value doesn't exist, the application is not set to run at startup
                    return false;
                }
                else
                {
                    // The value exists, the application is set to run at startup
                    return true;
                }
            }
            catch
            {
                //No admin rights, cant check, return false
                return false;
            }
        }

        //Simple toggle for the RUn at startup , admin privs required
        private void toggleIt()
        {
            try
            {
                if (!checkStatus())
                {
                    // Add the value in the registry so that the application runs at startup
                    rkApp.SetValue(myApp, Application.ExecutablePath.ToString() + " 1");
                }
                else
                {
                    // Remove the value from the registry so that the application doesn't start
                    rkApp.DeleteValue(myApp, false);
                }
            }
            catch
            {
                //no admin privs, do nothing but alert the user
                MessageBox.Show("You attemped to modify the launch settings of this tool but you do not have the required permissions to access the registry", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }        

        //This bad boy will kill everything running so threads dont continue to loop after the user requests to quit
        private void destroyThread(bool exit)
        {
            try
            {
                waveOut.Stop();
                if (exit)
                    waveOut.Dispose();
            }
            catch { }
            if (exit)
            {
                backgroundWorker1.CancelAsync();
                timeToQuit = true;
            }
            else          
                timeToBreak = !exit;

            //GC.Collect();
        }

        public bool newCheckAudio()
        {
            bool audioPlaying = false;
            try
            {
                using (sessionManager = GetDefaultAudioSessionManager2(DataFlow.Render))
                {
                    Thread.Sleep(1);
                    using (var sessionEnumerator = sessionManager.GetSessionEnumerator())
                    {
                        Thread.Sleep(1);
                        foreach (var session in sessionEnumerator)
                        {
                            Thread.Sleep(1);
                            if (session.SessionState == AudioSessionState.AudioSessionStateActive)
                            {
                                
                                using (audioMeterInformation = session.QueryInterface<AudioMeterInformation>())
                                {
                                    Thread.Sleep(1);
                                    float audioLevel = audioMeterInformation.GetPeakValue() * 100;

                                    if (audioLevel > 0.01f)
                                    {
                                        using (var session2 = session.QueryInterface<AudioSessionControl2>())
                                            setLabel8(string.Format("Program: {0}", session2.Process.MainWindowTitle ?? String.Empty));
                                        audioPlaying = true;
                                        break;
                                    }
                                }
                            }
                            Thread.Sleep(2);
                        }
                    }
                }
            }
            catch { }

            
            return audioPlaying;
        }
            
        //This is the bread and butter for audio checking, it's now set to check for an audio streaming application and if it is detected, the volume at that moment must be above 0.0f volume or it will not detect.
        public bool checkAudio()
        {            
            using (var sessionManager = GetDefaultAudioSessionManager2(DataFlow.Render))
            {
                using (var sessionEnumerator = sessionManager.GetSessionEnumerator())
                {
                    foreach (var session in sessionEnumerator)
                    {
                        if (session.SessionState == AudioSessionState.AudioSessionStateActive)
                        {
                            using (var audioMeterInformation = session.QueryInterface<AudioMeterInformation>())
                            {
                                float audioLevel = audioMeterInformation.GetPeakValue() * 100;

                                if (audioLevel > 0.01f)
                                {
                                    using (var session2 = session.QueryInterface<AudioSessionControl2>())
                                        setLabel8(string.Format("Program: {0}", session2.Process.MainWindowTitle ?? String.Empty));
                                   
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            
            return false;
        }

        //Method to acccess device information for audio
        private static AudioSessionManager2 GetDefaultAudioSessionManager2(DataFlow dataFlow)
        {
            using (var enumerator = new MMDeviceEnumerator())
            {
                using (var device = enumerator.GetDefaultAudioEndpoint(dataFlow, Role.Multimedia))
                {
                    //Console.WriteLine("DefaultDevice: " + device.FriendlyName);
                    var sessionManager = AudioSessionManager2.FromMMDevice(device);
                    return sessionManager;
                }
            }
        }

        private void initializeAudioStream()
        {           
            if (Properties.Settings.Default.whitenoise)
            {
                nextSound = "Beep";
                mp3Reader = whiteNoiseReader;
                waveOut.Init(whiteNoiseBlockAlignStream);
            }
            else
            {
                nextSound = "WhiteNoise";
                mp3Reader = beepReader;
                waveOut.Init(beepBlockAlignStream);
            }

            button2.Text = string.Format("Switch Sound to {0}", nextSound);
            numericUpDown2.Value = (decimal)Properties.Settings.Default.volume;
            numericUpDown3.Value = Properties.Settings.Default.length;

            playAudio(false);
        }

        /// <summary>
        /// Plays the specified emebeded resource a wav to keep the headphones alive
        /// </summary>
        private void playAudio(bool updateText)
        {
            if (updateText)
                setLabel2("Status: Audio was not detected, playing audio");
            try
            {
                //mp3file.Position = 0;
                mp3Reader.Position = 0;
                
                waveOut.Volume = Properties.Settings.Default.volume;
                waveOut.Play();
                Thread.Sleep((int)Properties.Settings.Default.length);
                waveOut.Stop();
            }
            catch
            {
                setLabel2("Status: Error playing audio, please wait");
            }
            //GC.Collect();
        }

        //Gets the bytes from the embeded file specified
        public Stream GetResourceStream(string filename)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string resname = asm.GetName().Name + "." + filename;
            return asm.GetManifestResourceStream(resname);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //performButton1();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            Properties.Settings.Default.whitenoise = !Properties.Settings.Default.whitenoise;
            Properties.Settings.Default.Save();
            
            initializeAudioStream();
            button2.Enabled = true;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.volume = (float)numericUpDown2.Value;
            Properties.Settings.Default.Save();
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.length = numericUpDown3.Value;
            Properties.Settings.Default.Save();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            GC.Collect();
        }
    }
}
